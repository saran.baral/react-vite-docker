export const config = {
    BASE_ENV: import.meta.env.VITE_BASE_URL,
    envMode: import.meta.env.VITE_ENV_MODE
}