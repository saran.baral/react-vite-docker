import Test from "./Test"

function App() {
  return (
    <>
      <h1 className="text-3xl font-bold uppercase text-slate-600 w-screen h-screen grid place-content-center">
        React Vite App
        <Test />
      </h1>
    </>
  )
}

export default App
