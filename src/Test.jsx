import { config } from '../config'
import { decrement, increment } from '../redux/slices/counter.slices';
import photo from '/delete.jpg'
import { useSelector, useDispatch } from "react-redux";


const Test = () => {

    const count = useSelector((state) => state.counter.count);
    console.log(useSelector((state) => state.counter));
    const dispatch = useDispatch();

    const incrementCount = () => {
        dispatch(increment());
    };

    const decrementCount = () => {
        dispatch(decrement());
    };

    console.log(config.envMode)
    return (
        <div>
            <h1>ENV MODE = {config.envMode}</h1>
            <img src={photo} alt="ok" />

            <div>
                <h1>Count: {count}</h1>
                <button onClick={incrementCount}>Increment</button>
                <button onClick={decrementCount}>Decrement</button>
            </div>
        </div>
    )
}

export default Test