## vite.config.js - Changes
export default defineConfig({
  plugins: [react()],
  server: {
    watch: {
      usePolling: true,
    },
    host: true, // needed for the Docker Container port mapping to work
    strictPort: true,
    port: 5173, // you can replace this port with any port
  }

## Create DockerFile
FROM node:16-alpine

WORKDIR /app

COPY package.json .

RUN npm install

COPY . .

EXPOSE 3000

CMD ["npm", "run", "dev"]   


## build docker file - from terminal
docker build -t [any name] .

## Check docker images
docker images

## Run docker container
docker run -d --rm -p 5173:5173 my-react-app //mr-react-app is image name




## docker-compose.yml
version: "3.8"

services:
  app:
    image: my-react-vite-app
    container_name: my-react-vite-app
    ports:
      - 5173:5173
    volumes:
      - .:/app/dist // Use /app only if u dont need to run build file
    build: //include this if build file also run through docker/ else remove this
      context: .
      dockerfile: Dockerfile
      target: development