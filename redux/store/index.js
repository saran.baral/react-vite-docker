import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "../reducers";
import { config } from "../../config";

export const store = configureStore({
  reducer: rootReducer,
  devTools: config.BASE_ENV !== 'production',
});

